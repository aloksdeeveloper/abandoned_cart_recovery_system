﻿using System;
using System.IO;

namespace CartRecovery.Common.Extensions
{
    public class TextLoggerExtension
    {
        private static readonly object LOCK = new object();
        private static readonly string logPath = string.Format("{0}\\Log", (object)AppDomain.CurrentDomain.BaseDirectory);

        static TextLoggerExtension()
        {
            if (Directory.Exists(logPath))
                return;
            Directory.CreateDirectory(logPath);
        }

        public static void Log(string message, string ClassName, string MethodDeclaringType, string logType = "Error")
        {
            lock (LOCK)
            {
                DateTime now = DateTime.Now;
                using (StreamWriter streamWriter1 = new StreamWriter(string.Format("{0}\\{1}Log-" + now.ToString("dd-MM-yyyy") + ".log", (object)logPath, (object)logType), true))
                {
                    message = string.Format("Class: {0}\nMethod: {1}\n{2} Message: {3}", ClassName, MethodDeclaringType, logType, message);
                    StreamWriter streamWriter2 = streamWriter1;
                    now = DateTime.Now;
                    string str = "\r" + now.ToString("dd/MM/yyyy hh:mm:ss tt");
                    streamWriter2.WriteLine(str);
                    streamWriter1.WriteLine(message);
                    streamWriter1.WriteLine("\n\r");
                    streamWriter1.Close();
                }
            }
        }
    }
}