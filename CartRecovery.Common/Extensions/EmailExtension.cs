﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using CartRecovery.Data.Domain;

using Microsoft.Extensions.Configuration;

namespace CartRecovery.Common.Extensions
{
    public class EmailExtension
    {
        #region Private member variables...

        private readonly IConfiguration _configuration;

        #endregion Private member variables...

        /// <summary>
        /// Public constructor.
        /// </summary>
        public EmailExtension(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public EmailExtension()
        {
        }

        public async Task<string> SendMail(Notification notification, MessageTemplate messageTemplate, bool IsBodyHtml = true, List<Attachment> allAttachments = null)
        {
            SmtpClient smtpClient = new SmtpClient();

            MailMessage mailMessage = new MailMessage();
            string Result = "";

            var basicCredential = new NetworkCredential(_configuration["Email:UserCredential"], _configuration["Email:PasswordCredential"]);

            smtpClient.Credentials = basicCredential;
            smtpClient.Host = _configuration["AppSettings:Host"];
            smtpClient.Port = Convert.ToInt32(_configuration["AppSettings:Port"]);
            smtpClient.Timeout = Convert.ToInt32(_configuration["AppSettings:Timeout"]);
            smtpClient.EnableSsl = true;

            try
            {
                MailAddress Sender = new MailAddress(messageTemplate.SenderEmail, messageTemplate.SenderDisplayName);
                mailMessage.From = Sender;

                string[] recipientEmails = null;

                if (string.IsNullOrEmpty(notification.RecipientEmail)) { }
                else
                {
                    recipientEmails = notification.RecipientEmail.Split(new Char[] { ',' });
                    foreach (string recipientEmail in recipientEmails)
                    {
                        if (ValidateEmailAddress(recipientEmail))
                        {
                            mailMessage.To.Add(recipientEmail);
                        }
                    }
                }
                if (string.IsNullOrEmpty(messageTemplate.ClientCopy)) { }
                else
                {
                    recipientEmails = messageTemplate.ClientCopy.Trim().Split(new Char[] { ',' });
                    foreach (string recipientEmail in recipientEmails)
                    {
                        if (ValidateEmailAddress(recipientEmail))
                        {
                            mailMessage.Bcc.Add(recipientEmail);
                        }
                    }
                }
                mailMessage.Subject = messageTemplate.Subject;

                if (allAttachments != null)
                {
                    foreach (var attachment in allAttachments)
                    {
                        mailMessage.Attachments.Add(attachment);
                    }
                }

                AlternateView plainView = AlternateView.CreateAlternateViewFromString(messageTemplate.Message, null, "text/html");

                mailMessage.AlternateViews.Add(plainView);
                mailMessage.IsBodyHtml = IsBodyHtml;
                mailMessage.BodyEncoding = System.Text.Encoding.UTF8;
                // await smtpClient.SendMailAsync(mailMessage);

                Result = "Sent";
            }
            catch (Exception exception)
            {
                Result = "Error: " + exception.Source + ' ' + exception.Message + ' ' + exception.InnerException; ;
                return Result;
            }
            finally
            {
                mailMessage.Dispose();
            }

            return Result;
        }

        public static string GetEmailTemplatePath()
        {
            string newPath = "";
            try
            {
                string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));

                if (path.Contains("bin") == true)
                {
                    var binLocation = path.Split(new[] { "bin" }, StringSplitOptions.None);

                    if (binLocation[0].EndsWith("\\") == true)
                        newPath = binLocation[0] + "bin\\Template";
                    else
                        newPath = binLocation[0] + "\\Template";

                    return newPath;
                }
                else
                {
                    if (path.EndsWith("\\") == true)
                        newPath = path + "Template";
                    else
                        newPath = path + "\\Template";

                    return newPath;
                }
            }
            catch (Exception exception)
            {
                return "Error: " + exception.Source + ' ' + exception.Message + ' ' + exception.InnerException;
            }
        }

        public static List<Attachment> GetEmailAttachments()
        {
            List<Attachment> allAttachments = new List<Attachment>();
            var emailTemplatePath = GetEmailTemplatePath();

            try
            {
                //Attachment attach0 = new Attachment(emailTemplatePath + "\\logo.png");
                //attach0.ContentId = "image000";

                //allAttachments.Add(attach0);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return allAttachments;
        }

        public static string ReplaceNotificationParameters(string messageTemplate, Dictionary<string, string> optionalParameters)
        {
            try
            {
                foreach (KeyValuePair<string, string> pair in optionalParameters)
                {
                    messageTemplate = messageTemplate.Replace("{" + pair.Key + "}", pair.Value);
                }

                return messageTemplate;
            }
            catch (Exception exception)
            {
                return "Error: " + exception.Source + ' ' + exception.Message + ' ' + exception.InnerException;
            }
        }

        // Email Format Validation
        public static bool ValidateEmailAddress(string emailAddress)
        {
            bool isValidated = false;
            if (Regex.IsMatch(emailAddress, "^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$"))
            {
                isValidated = true;
            }
            return isValidated;
        }
    }
}