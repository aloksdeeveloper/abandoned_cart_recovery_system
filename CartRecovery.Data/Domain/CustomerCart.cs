﻿using System;

namespace CartRecovery.Data.Domain
{
    public class CustomerCart
    {
        /// <summary>
        /// The parent node identifier.
        /// </summary>
        public int CustomerCartID { get; set; }

        /// <summary>
        /// The customer's unique email.
        /// </summary>
        public string CustomerEmail { get; set; }

        /// <summary>
        /// The customer username.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// The customer cart items.
        /// </summary>
        public string CartItems { get; set; }

        /// <summary>
        /// Flag for before or after checkout point.
        /// </summary>
        public bool IsCheckoutPoint { get; set; }

        /// <summary>
        /// Flag for if mail is profiled for sending for the present cart.
        /// </summary>
        public bool MailSent { get; set; }

        /// <summary>
        /// Flag for if customer cart has been deleted.
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// The date that the mail was sent.
        /// </summary>
        public DateTime SentDate { get; set; }

        /// <summary>
        /// The date that the present cart was created.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// The date that the customer logged in last or modified the cart.
        /// </summary>
        public DateTime ModifiedDate { get; set; }
    }
}