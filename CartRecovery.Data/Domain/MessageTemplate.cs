﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace CartRecovery.Data.Domain
{
    public class MessageTemplate
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MessageTemplateID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        /// <summary>
        /// The email sender Email address.
        /// </summary>
        public string SenderEmail { get; set; }

        /// <summary>
        /// The Email Sender display name.
        /// </summary>
        public string SenderDisplayName { get; set; }

        public string Message { get; set; }

        public string Subject { get; set; }

        public string ClientCopy { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}