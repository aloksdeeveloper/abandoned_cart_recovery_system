﻿using System;

namespace CartRecovery.Data.Domain
{
    public class Notification
    {
        /// <summary>
        /// The parent node identifier.
        /// </summary>
        public int NotificationID { get; set; }

        public string UserName { get; set; }

        /// <summary>
        /// The customer's unique email.
        /// </summary>
        public string RecipientEmail { get; set; }

        /// <summary>
        /// The customer's cart items.
        /// </summary>
        public string CartItems { get; set; }

        /// <summary>
        /// Flag for if mail is successfully sent.
        /// </summary>
        public bool IsSent { get; set; }

        /// <summary>
        /// The date if the customer logs in after the mail.
        /// </summary>
        public DateTime LoginAfterMail { get; set; }

        /// <summary>
        /// The date if the customer deletes cart after the mail.
        /// </summary>
        public DateTime DeleteAfterMail { get; set; }

        /// <summary>
        /// The date the notification was created.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// The date the notification was last updated.
        /// </summary>
        public DateTime ModifiedDate { get; set; }
    }
}