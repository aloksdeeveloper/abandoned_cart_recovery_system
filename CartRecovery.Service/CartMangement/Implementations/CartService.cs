﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using AutoMapper;

using CartRecovery.Common.Extensions;
using CartRecovery.Data.Domain;
using CartRecovery.Service.CartMangement.Entity;
using CartRecovery.Service.CartMangement.Interfaces;
using CartRecovery.Service.HangFire_Notification.Interfaces;
using Newtonsoft.Json;

namespace CartRecovery.Service.CartMangement.Implementations
{
    public class CartService : ICartService
    {
        private readonly ICartRepo _cartRepo;
        private readonly INotificationRepo _notificationRepo;

        public CartService(ICartRepo cartRepo)
        {
            _cartRepo = cartRepo;
        }

        public CustomerCartEntity GetCustomerCart(string emailAddress)
        {
            var customerCartModel = _cartRepo.GetCustomerCart(emailAddress);

            if (!customerCartModel.MailSent && (customerCartModel.SentDate.AddDays(2) >= DateTime.Now.Date))
            {
                var notification = _notificationRepo.GetNotificationByEmail(emailAddress);
                notification.LoginAfterMail = DateTime.Now;
                _notificationRepo.UpdateNotificationStatus(notification);
            }
            var customerCart = Mapper.Map<CustomerCartEntity>(customerCartModel);

            var cartItems = JsonConvert.DeserializeObject<List<CartItem>>(customerCartModel.CartItems);
            customerCart.CartItems = cartItems;

            return customerCart;
        }

        public Task<bool> SaveAndModifyCustomerCart(CustomerCartEntity customerCart)
        {
            var customerCartModel = new CustomerCart();

            customerCartModel = _cartRepo.GetCustomerCart(customerCart.CustomerEmail);

            customerCartModel = Mapper.Map(customerCart, customerCartModel);

            if (customerCartModel.CustomerCartID == 0)
            {
                customerCartModel.CreatedDate = DateTime.Now;
            }
            else
            {
                customerCartModel.MailSent = false;
            }

            customerCartModel.CartItems = JsonConvert.SerializeObject(customerCart.CartItems);

            var isSuccessful = _cartRepo.SaveAndModifyCustomerCart(customerCartModel);
            return isSuccessful;
        }

        public async Task<bool> DeleteCustomerCartAsync(string emailAddress)
        {
            var customerCartModel = _cartRepo.GetCustomerCart(emailAddress);

            if (customerCartModel == null)
            {
                return false;
            }
            if (!customerCartModel.MailSent && (customerCartModel.SentDate.AddDays(2) >= DateTime.Now.Date))
            {
                var notification = _notificationRepo.GetNotificationByEmail(emailAddress);
                notification.LoginAfterMail = DateTime.Now;
                var updateResponse = _notificationRepo.UpdateNotificationStatus(notification);
            }

            customerCartModel.IsDeleted = true;
            customerCartModel.MailSent = false;
            var isSuccessful = await _cartRepo.SaveAndModifyCustomerCart(customerCartModel);
            return isSuccessful;
        }
    }
}