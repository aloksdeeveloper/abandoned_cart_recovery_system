﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using CartRecovery.Data.Context;
using CartRecovery.Data.Domain;
using CartRecovery.Service.CartMangement.Interfaces;

using Microsoft.EntityFrameworkCore;

namespace CartRecovery.Service.CartMangement.Implementations
{
    public class CartRepo : ICartRepo
    {
        private readonly CartRecoveryContext _context;

        public CartRepo(CartRecoveryContext context)
        {
            _context = context;
        }

        public CustomerCart GetCustomerCart(string customerEmail)
        {
            try
            {
                var customerCart = _context.Set<CustomerCart>().Where(x => (x.CustomerEmail == customerEmail) && (x.IsDeleted == false)).FirstOrDefault();
                return customerCart;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> SaveAndModifyCustomerCart(CustomerCart customerCart)
        {
            bool isSuccessful = false;
            if (customerCart.CustomerCartID > 0)
            {
                _context.Set<CustomerCart>().Attach(customerCart);
                _context.Entry(customerCart).State = EntityState.Modified;
                _context.SaveChanges();

                isSuccessful = true;
            }
            else
            {
                _context.Set<CustomerCart>().Add(customerCart);
                _context.SaveChanges();

                isSuccessful = customerCart.CustomerCartID > 0 ? true : false;
            }

            return await Task.Run(() => isSuccessful);
        }

        public async Task<List<CustomerCart>> GetAbandonedCart(double daysIdle)
        {
            var customerCarts = _context.Set<CustomerCart>().Where(x => (!x.IsDeleted) && (x.ModifiedDate.Date.AddDays(daysIdle) == DateTime.Now.Date)).ToList();

            return customerCarts;
        }
    }
}