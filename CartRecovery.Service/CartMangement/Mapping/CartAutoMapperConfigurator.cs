﻿using System;

using AutoMapper;

using CartRecovery.Data.Domain;
using CartRecovery.Service.CartMangement.Entity;

namespace CartRecovery.Service.CartMangement.Mapping
{
    public class CartAutoMapperConfigurator : Profile
    {
        public CartAutoMapperConfigurator()
        {
            CreateMap<CustomerCartEntity, CustomerCart>()
                .ForMember(x => x.CustomerCartID, options => options.Ignore())
                .ForMember(x => x.CustomerEmail, options => options
                    .MapFrom(s => s.CustomerEmail ?? ""))
                .ForMember(x => x.Username, options => options
                    .MapFrom(s => s.CustomerUsername ?? ""))
                .ForMember(x => x.CartItems, options => options.Ignore())
                .ForMember(x => x.IsCheckoutPoint, options => options
                    .MapFrom(s => s.IsCheckoutPoint))
                .ForMember(x => x.MailSent, options => options.Ignore())
                .ForMember(x => x.SentDate, options => options.Ignore())
                .ForMember(x => x.IsDeleted, options => options.Ignore())
                .ForMember(x => x.CreatedDate, options => options.Ignore())
                .ForMember(x => x.ModifiedDate, options => options
                    .MapFrom(s => DateTime.Now));

            CreateMap<CustomerCart, CustomerCartEntity>()
                .ForMember(x => x.CustomerEmail, options => options
                    .MapFrom(s => s.CustomerEmail ?? ""))
                .ForMember(x => x.CustomerUsername, options => options
                    .MapFrom(s => s.Username ?? ""))
                .ForMember(x => x.CartItems, options => options.Ignore())
                .ForMember(x => x.IsCheckoutPoint, options => options
                    .MapFrom(s => s.IsCheckoutPoint));
        }
    }
}