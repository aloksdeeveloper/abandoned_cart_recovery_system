﻿using System.Threading.Tasks;

using CartRecovery.Service.CartMangement.Entity;

namespace CartRecovery.Service.CartMangement.Interfaces
{
    public interface ICartService
    {
        CustomerCartEntity GetCustomerCart(string emailAddress);

        Task<bool> SaveAndModifyCustomerCart(CustomerCartEntity customerCart);

        Task<bool> DeleteCustomerCartAsync(string emailAddress);
    }
}