﻿using System.Collections.Generic;
using System.Threading.Tasks;

using CartRecovery.Data.Domain;

namespace CartRecovery.Service.CartMangement.Interfaces
{
    public interface ICartRepo
    {
        CustomerCart GetCustomerCart(string emailAddress);

        Task<bool> SaveAndModifyCustomerCart(CustomerCart customerCart);

        Task<List<CustomerCart>> GetAbandonedCart(double daysIdle);
    }
}