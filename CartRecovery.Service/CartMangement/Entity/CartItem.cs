﻿using System.ComponentModel.DataAnnotations;

namespace CartRecovery.Service.CartMangement.Entity
{
    public class CartItem
    {
        /// <summary>
        /// The name of the item
        /// </summary>
        [Required]
        public string ItemName { get; set; }

        /// <summary>
        /// The unique identifier of the Item
        /// </summary>
        [Required]
        public string ItemID { get; set; }

        /// <summary>
        /// The number of the amount of the item the customer wants
        /// </summary>
        [Required]
        public int Count { get; set; }
    }
}