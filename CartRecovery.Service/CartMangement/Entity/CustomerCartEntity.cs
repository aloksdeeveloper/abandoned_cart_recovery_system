﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CartRecovery.Service.CartMangement.Entity
{
    public class CustomerCartEntity
    {
        /// <summary>
        ///
        /// </summary>
        [Required]
        public string CustomerEmail { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Required]
        public string CustomerUsername { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Required]
        public bool IsCheckoutPoint { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Required]
        public IList<CartItem> CartItems { get; set; }
    }
}