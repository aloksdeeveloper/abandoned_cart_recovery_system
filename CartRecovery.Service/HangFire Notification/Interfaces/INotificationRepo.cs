﻿using System.Collections.Generic;
using System.Threading.Tasks;

using CartRecovery.Data.Domain;

namespace CartRecovery.Service.HangFire_Notification.Interfaces
{
    public interface INotificationRepo
    {
        Task<bool> SaveNotificationBatch(Notification[] notifications);

        List<Notification> GetNotifications();

        Notification GetNotificationByEmail(string customerEmail);

        MessageTemplate GetMessageTemplate(string templateName);

        Task<bool> UpdateNotificationStatus(Notification notification);
    }
}