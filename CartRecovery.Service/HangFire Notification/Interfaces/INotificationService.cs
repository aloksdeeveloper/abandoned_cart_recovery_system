﻿namespace CartRecovery.Service.HangFire_Notification.Interfaces
{
    public interface INotificationService
    {
        void GetAbandonedCartForNotification();

        void SendNotification();
    }
}