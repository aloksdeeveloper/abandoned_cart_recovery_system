﻿using System;

using AutoMapper;

using CartRecovery.Data.Domain;

namespace CartRecovery.Service.HangFire_Notification.Mapping
{
    public class NotificationAutoMapperConfigurator : Profile
    {
        public NotificationAutoMapperConfigurator()
        {
            CreateMap<CustomerCart, Notification>()
                .ForMember(x => x.NotificationID, options => options.Ignore())
                .ForMember(x => x.RecipientEmail, options => options
                    .MapFrom(s => s.CustomerEmail ?? ""))
                .ForMember(x => x.UserName, options => options
                    .MapFrom(s => s.Username ?? ""))
                .ForMember(x => x.CartItems, options => options
                    .MapFrom(s => s.CartItems ?? ""))
                .ForMember(x => x.IsSent, options => options
                    .MapFrom(s => false))
                .ForMember(x => x.LoginAfterMail, options => options.Ignore())
                .ForMember(x => x.DeleteAfterMail, options => options.Ignore())
                .ForMember(x => x.CreatedDate, options => options
                    .MapFrom(s => DateTime.Now))
                .ForMember(x => x.ModifiedDate, options => options
                    .MapFrom(s => DateTime.Now));
        }
    }
}