﻿using System;
using System.Collections.Generic;
using System.Text;
using CartRecovery.Service.HangFire_Notification.Implementations;
using CartRecovery.Service.HangFire_Notification.Interfaces;
using Hangfire;
using Microsoft.Extensions.Configuration;

namespace CartRecovery.Service.HangFire_Notification
{
    public class CronJobs
    {
        private readonly IConfiguration _configuration;
        private readonly INotificationService _notificationService;

        public CronJobs(IConfiguration configuration, INotificationService notificationService)
        {
            _configuration = configuration;
            _notificationService = notificationService;
        }

        public void NotificationCRON()
        {
            RecurringJob.AddOrUpdate("NotificationCRON", () => _notificationService.GetAbandonedCartForNotification(), _configuration["CRONJob:GetNotificationCRON"]);
        }

        public void SendMailCRON()
        {
            RecurringJob.AddOrUpdate("SendMailCRON", () => _notificationService.SendNotification(), _configuration["CRONJob:SendMailCRON"]);
        }
    }
}