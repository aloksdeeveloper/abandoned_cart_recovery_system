﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using CartRecovery.Data.Context;
using CartRecovery.Data.Domain;
using CartRecovery.Service.HangFire_Notification.Interfaces;

using Microsoft.EntityFrameworkCore;

namespace CartRecovery.Service.HangFire_Notification.Implementations
{
    public class NotificationRepo : INotificationRepo
    {
        private readonly CartRecoveryContext _context;

        public NotificationRepo(CartRecoveryContext context)
        {
            _context = context;
        }

        public async Task<bool> SaveNotificationBatch(Notification[] notifications)
        {
            bool isSuccessful = false;

            _context.Notifications.AddRange(notifications);
            _context.SaveChanges();

            return await Task.Run(() => isSuccessful);
        }

        public List<Notification> GetNotifications()
        {
            try
            {
                var notifications = _context.Set<Notification>().Where(x => !x.IsSent).ToList();

                return notifications;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Notification GetNotificationByEmail(string customerEmail)
        {
            try
            {
                var notification = _context.Set<Notification>().Where(x => (x.RecipientEmail == customerEmail) && (x.IsSent == true)).OrderByDescending(x => x.NotificationID).FirstOrDefault();
                return notification;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public MessageTemplate GetMessageTemplate(string templateName)
        {
            try
            {
                var messageTemplate = _context.Set<MessageTemplate>().Where(x => x.Name == templateName).FirstOrDefault();
                return messageTemplate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> UpdateNotificationStatus(Notification notification)
        {
            _context.Set<Notification>().Attach(notification);
            _context.Entry(notification).State = EntityState.Modified;
            _context.SaveChanges();

            return await Task.Run(() => true);
        }
    }
}