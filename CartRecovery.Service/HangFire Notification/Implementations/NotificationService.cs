﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;

using AutoMapper;

using CartRecovery.Common.Extensions;
using CartRecovery.Data.Domain;
using CartRecovery.Service.CartMangement.Entity;
using CartRecovery.Service.CartMangement.Interfaces;
using CartRecovery.Service.HangFire_Notification.Interfaces;

using Microsoft.Extensions.Configuration;

using Newtonsoft.Json;

namespace CartRecovery.Service.HangFire_Notification.Implementations
{
    public class NotificationService : INotificationService
    {
        private readonly IConfiguration _configuration;
        private readonly ICartRepo _cartRepo;
        private readonly INotificationRepo _notificationRepo;
        private EmailExtension emailExtension = new EmailExtension();

        public NotificationService(IConfiguration configuration, ICartRepo cartRepo, INotificationRepo notificationRepo)
        {
            _configuration = configuration;
            _cartRepo = cartRepo;
            _notificationRepo = notificationRepo;
        }

        public void GetAbandonedCartForNotification()
        {
            var beforeCheckout = Convert.ToDouble(_configuration["AppSettings:BeforeCheckoutDays"]);
            var atCheckout = Convert.ToDouble(_configuration["AppSettings:AtCheckoutDays"]);

            var abandonedCarts = _cartRepo.GetAbandonedCart(beforeCheckout).Result;
            var notifications = Mapper.Map<List<CustomerCart>, List<Notification>>(abandonedCarts).ToArray();
            var saveResult = _notificationRepo.SaveNotificationBatch(notifications);
            foreach (var abandonedCart in abandonedCarts)
            {
                abandonedCart.MailSent = true;
                abandonedCart.SentDate = DateTime.Now;
                _cartRepo.SaveAndModifyCustomerCart(abandonedCart);
            }

            abandonedCarts = _cartRepo.GetAbandonedCart(atCheckout).Result;
            notifications = Mapper.Map<List<CustomerCart>, List<Notification>>(abandonedCarts).ToArray();
            saveResult = _notificationRepo.SaveNotificationBatch(notifications);
            foreach (var abandonedCart in abandonedCarts)
            {
                abandonedCart.MailSent = true;
                abandonedCart.SentDate = DateTime.Now;
                _cartRepo.SaveAndModifyCustomerCart(abandonedCart);
            }
        }

        public void SendNotification()
        {
            var notifications = _notificationRepo.GetNotifications();
            var messageTemplate = _notificationRepo.GetMessageTemplate("AlertTemplate");

            foreach (var notification in notifications)
            {
                var cartItems = JsonConvert.DeserializeObject<List<CartItem>>(notification.CartItems);
                var emailResponse = SendMailJob(cartItems, notification, messageTemplate);
                notification.IsSent = emailResponse.Result;

                _notificationRepo.UpdateNotificationStatus(notification);
            }
        }

        public async Task<bool> SendMailJob(List<CartItem> cartItems, Notification notification, MessageTemplate messageTemplate)
        {
            try
            {
                string emailBody = "";
                var emailTemplatePath = EmailExtension.GetEmailTemplatePath();

                if (emailTemplatePath.ToString().Contains("Error") == true)
                {
                    TextLoggerExtension.Log($"Failure - {emailTemplatePath.ToString()}", GetType().Name, MethodBase.GetCurrentMethod().Name, "Error");
                    return false;
                };

                Dictionary<string, string> OptionalParameters = new Dictionary<string, string>()
                {
                    {"UserName", notification.UserName}
                };

                if (cartItems.Count != 0)
                {
                    string ItemEntity = "";
                    foreach (var cartItem in cartItems)
                    {
                        ItemEntity = ItemEntity + "\n" + $"Item Name: {cartItem.ItemName} - Count: {cartItem.Count}";
                    }

                    OptionalParameters.Add("ItemEntity", ItemEntity);
                }

                emailBody = EmailExtension.ReplaceNotificationParameters(emailBody, OptionalParameters);
                if (emailBody.Contains("Error") == true)
                    return false;

                var attachments = EmailExtension.GetEmailAttachments();
                var emailResult = await new EmailExtension().SendMail(notification, messageTemplate, true, attachments);
                if (emailResult != "Sent")
                {
                    TextLoggerExtension.Log($"Failure - {emailResult}", GetType().Name, MethodBase.GetCurrentMethod().Name, "Error");
                    return false;
                }

                return true;
            }
            catch (Exception exception)
            {
                TextLoggerExtension.Log($"Exception: {exception.ToString()}", GetType().Name, MethodBase.GetCurrentMethod().Name, "Error");
                return false;
            }
        }
    }
}