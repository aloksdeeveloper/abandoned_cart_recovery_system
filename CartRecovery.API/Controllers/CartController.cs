﻿using System;
using System.Threading.Tasks;

using CartRecovery.Service.CartMangement.Entity;
using CartRecovery.Service.CartMangement.Interfaces;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CartRecovery.API.Controllers
{
    /// <summary>
    /// <c>CartController</c> Offers access to client application
    /// Author: John Alokan
    /// </summary>
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerCartController : ControllerBase
    {
        private readonly ICartService _cartServices;

        /// <summary>
        /// Controller constructor gets called by client. Use this method to reference injected services.
        /// </summary>
        /// <param name="cartServices"></param>
        /// <returns></returns>
        public CustomerCartController(ICartService cartServices)
        {
            _cartServices = cartServices;
        }

        /// <summary>
        /// Endpoint to get specific cart by email Address
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns>CustomerCartEntity</returns>
        // GET: api/CustomerCart/GetCartByEmailAddress
        [HttpGet("GetCartByEmailAddress/{emailAddress}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(CustomerCartEntity), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<CustomerCartEntity> GetCartByEmailAddress(string emailAddress)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        status = 400,
                        message = "Something went wrong! No data was submitted",
                        ModelState
                    });
                }

                if (string.IsNullOrEmpty(emailAddress))
                {
                    return BadRequest(new
                    {
                        status = 400,
                        message = "Something went wrong! No data was submitted"
                    });
                }

                var bank = _cartServices.GetCustomerCart(emailAddress);

                if (bank != null)
                {
                    return Ok(new
                    {
                        status = 200,
                        message = "Record Found!",
                        bank
                    });
                }
                else
                {
                    return NotFound(new
                    {
                        status = 404,
                        message = "Record Not Found! The requested information does NOT exist or has been deleted",
                        bank
                    });
                }
            }
            catch (Exception exception)
            {
                return NotFound(new
                {
                    status = 404,
                    message = "Something went wrong! See error log for detailed error information",
                    error = "Error: " + exception.Source + ' ' + exception.Message
                });
            }
        }

        /// <summary>
        /// Endpoint to create and modify a customer cart
        /// </summary>
        /// <param name="customerCart"></param>
        /// <returns>string</returns>
        // POST: api/CustomerCart/UpdateCart
        [HttpPost("PostCustomerCart")]
        [Produces("application/json", Type = typeof(string))]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PostCustomerCart([FromBody]CustomerCartEntity customerCart)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        status = 400,
                        message = "Something went wrong! No data was submitted",
                        ModelState
                    });
                }

                if (customerCart == null)
                {
                    return BadRequest(new
                    {
                        status = 400,
                        message = "Something went wrong! No data was submitted"
                    });
                }

                // Now submit the record for registration
                var taskSuccessful = await _cartServices.SaveAndModifyCustomerCart(customerCart);

                if (taskSuccessful)
                {
                    return Ok(new
                    {
                        status = 200,
                        message = "Your data was updated successfully",
                    });
                }
                else
                {
                    return NotFound(new
                    {
                        status = 404,
                        message = "Your data was NOT updated successfully",
                    });
                }
            }
            catch (Exception exception)
            {
                return NotFound(new
                {
                    status = 404,
                    message = "Something went wrong! See error log for detailed error information",
                    error = "Error: " + exception.Source + ' ' + exception.Message
                });
            }
        }

        /// <summary>
        /// Endpoint to delete the customer cart by emailAddress
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns>string</returns>
        // DELETE: api/CustomerCart/DeleteCart/emailaddress
        [HttpDelete("DeleteCart/{emailAddress}")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DeleteCart(string emailAddress)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        status = 400,
                        message = "Something went wrong! No data was submitted",
                        ModelState
                    });
                }

                if (string.IsNullOrEmpty(emailAddress))
                {
                    return BadRequest(new
                    {
                        status = 400,
                        message = "Something went wrong! No data was submitted"
                    });
                }

                var isDeleted = await _cartServices.DeleteCustomerCartAsync(emailAddress);

                if (isDeleted)
                {
                    return Ok(new
                    {
                        status = 200,
                        message = "The specified customer cart was deleted successfully"
                    });
                }
                else
                {
                    return NotFound(new
                    {
                        status = 404,
                        message = "The specified customer cart delete task was not successful"
                    });
                }
            }
            catch (Exception exception)
            {
                return NotFound(new
                {
                    status = 404,
                    message = "Something went wrong! See error log for detailed error information",
                    error = "Error: " + exception.Source + ' ' + exception.Message
                });
            }
        }
    }
}