﻿using CartRecovery.Service.CartMangement.Implementations;
using CartRecovery.Service.CartMangement.Interfaces;
using CartRecovery.Service.HangFire_Notification;
using CartRecovery.Service.HangFire_Notification.Implementations;
using CartRecovery.Service.HangFire_Notification.Interfaces;

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace CartRecovery.API
{
    public static class ServiceExtensions
    {
        public static IServiceCollection RegisterServices(this IServiceCollection services)
        {
            // Register all other services here. ddf
            services.AddTransient<CronJobs>();

            services.AddTransient<ICartService, CartService>();
            services.AddTransient<ICartRepo, CartRepo>();
            services.AddTransient<INotificationService, NotificationService>();
            services.AddTransient<INotificationRepo, NotificationRepo>();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            return services;
        }
    }
}