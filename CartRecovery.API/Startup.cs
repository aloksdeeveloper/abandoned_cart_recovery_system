﻿using System;
using System.IO;
using System.Reflection;

using CartRecovery.Data.Context;
using CartRecovery.Service.HangFire_Notification;
using Hangfire;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Swashbuckle.AspNetCore.Swagger;

namespace CartRecovery.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.RegisterServices();
            services.AddHangfire(x =>
            x.UseSqlServerStorage(Configuration.GetConnectionString("CartRecoveryConnection")));
            //This line adds Swagger generation services to our container.
            services.AddSwaggerGen(c =>
            {
                //Locate the XML file being generated by ASP.NET...
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.XML";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);

                //... and tell Swagger to use those XML comments.
                c.IncludeXmlComments(xmlPath);

                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Abandoned Cart Recovery System",
                    Description = "Cart management system for Hirefit",
                    TermsOfService = "None",
                    Contact = new Contact
                    {
                        Name = "Alokan John Ayodeji",
                        Email = "ayodeji.alokan@gmail.com",
                        Url = "http://google.com"
                    },
                    License = new License
                    {
                        Name = "Use under LICX",
                        Url = "http://www.google.com"
                    }
                });
            });
            string connection = Configuration.GetConnectionString("CartRecoveryConnection");

            services.AddDbContext<CartRecoveryContext>(options => options.UseSqlServer(connection, b => b.MigrationsAssembly("CartRecovery.Data")), ServiceLifetime.Transient);

            services.AddCors(option =>
            {
                option.AddPolicy("CorsPolicy",
                    builder => builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials()
                        );
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, CronJobs JobService)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            AutoMapperConfig.Configure();
            app.UseCors("CorsPolicy");
            app.UseHttpsRedirection();
            app.UseMvc();
            app.UseHangfireServer();
            app.UseHangfireDashboard("/Hangfire");
            DefaultData.Initialize(app);
            JobService.NotificationCRON();
            JobService.SendMailCRON();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Cart recovery API V1");
            });
        }
    }
}