﻿using System;
using System.Collections.Generic;
using System.Linq;

using CartRecovery.Data.Context;
using CartRecovery.Data.Domain;
using CartRecovery.Service.HangFire_Notification;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace CartRecovery.API
{
    [System.Runtime.InteropServices.Guid("E3C15055-710B-4B1B-AADB-C87F0697A6E5")]
    public class DefaultData
    {
        /// <summary>
        /// <c>Initialize</c> Offers extension for default data initialization
        /// </summary>
        /// <param name="app"></param>
        public static void Initialize(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                using (var context = serviceScope.ServiceProvider.GetRequiredService<CartRecoveryContext>())
                {
                    context.Database.EnsureCreated();

                    //Ppopulates the Message Template table if null
                    if (context.MessageTemplates != null && context.MessageTemplates.Any())
                    {
                        return;
                    }
                    else
                    {
                        var templates = LoadDefaultMessageTemplates().ToArray();
                        context.MessageTemplates.AddRange(templates);
                        context.SaveChanges();
                    }
                }

                //Continue application load
                return;
            }
        }

        /// <summary>
        /// Populates the message template table with default data
        /// </summary>
        /// <returns></returns>
        public static List<MessageTemplate> LoadDefaultMessageTemplates()
        {
            string CustomerCartTemplate = @"<table style=""border-spacing:0;border-collapse:collapse;vertical-align:top;background-color:transparent"" cellpadding=""0"" cellspacing=""0"" align=""center"" width=""100%"" border=""0"">
										<tbody>
											<tr style=""vertical-align:top"">
												<td style=""word-break:break-word;border-collapse:collapse!important;vertical-align:top"" width=""100%"">
													<table class=""m_4367086298119325691container"" style=""border-spacing:0;border-collapse:collapse;vertical-align:top;max-width:420px;margin:0 auto;text-align:inherit"" cellpadding=""0"" cellspacing=""0"" align=""center"" width=""100%"" border=""0"">
														<tbody>
															<tr style=""vertical-align:top"">
																<td style=""word-break:break-word;border-collapse:collapse!important;vertical-align:top"" width=""100%"">
																	<table class=""m_4367086298119325691block-grid"" style=""border-spacing:0;border-collapse:collapse;vertical-align:top;width:100%;max-width:420px;color:#000000;background-color:transparent"" cellpadding=""0"" cellspacing=""0"" width=""100%"" bgcolor=""transparent"">
																		<tbody>
																			<tr style=""vertical-align:top"">
																				<td style=""word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:center;font-size:0"">
																					<div class=""m_4367086298119325691col m_4367086298119325691num12"" style=""display:inline-block;vertical-align:top;width:420px"">
																						<table style=""border-spacing:0;border-collapse:separate;vertical-align:top"" cellpadding=""0"" cellspacing=""0"" align=""center"" width=""100%"" border=""0"">
																							<tbody>
																								<tr style=""vertical-align:top"">
																									<td style=""word-break:break-word;border-collapse:collapse!important;vertical-align:top;background-color:transparent;padding-top:0;padding-right:0;padding-bottom:30px;padding-left:0;border-top:1px solid #ededed;border-right:1px solid #ededed;border-bottom:1px solid #ededed;border-left:1px solid #ededed"">
																										<table style=""border-spacing:0;border-collapse:collapse;vertical-align:top;background:#fdfdfd;margin-bottom:20px"" cellpadding=""0"" cellspacing=""0"" width=""100%"">
																											<tbody>
																												<tr style=""vertical-align:top"">
																													<td style=""word-break:break-word;border-collapse:collapse!important;vertical-align:top;padding-top:40px;padding-right:10px;padding-bottom:0px;padding-left:10px"">
																														<div style=""color:#0d3e65;line-height:150%;font-family:-apple-system,BlinkMacSystemFont,&#39;Segoe UI&#39;,&#39;Roboto&#39;,&#39;Oxygen&#39;,&#39;Ubuntu&#39;,&#39;Cantarell&#39;,&#39;Fira Sans&#39;,&#39;Droid Sans&#39;,&#39;Helvetica Neue&#39;,sans-serif"">
																															<div style=""padding-bottom:30px"">
																																<p style=""margin:0 auto;max-width:220px;font-size:12px;line-height:20px;text-align:center"">
																																	<span style=""font-size:16px;line-height:20px"">Hi{UserName}, you have some items in your cart left in your cart that can brighten up your day</span>
																																</p>
																																<p style=""margin:0;font-size:14px;line-height:15px;text-align:center;padding-top:10px"">
																																	<strong>
																																		<span style=""line-height:30px;font-size:28px"">{ItemEntity}</span>
																																	</strong>
																																	<br>
																																</p>
																															</div>
																														</div>
																													</td>
																												</tr>

																											</tbody>
																									<table style=""border-spacing:0;border-collapse:collapse;vertical-align:top;margin-top:0;font-family:-apple-system,BlinkMacSystemFont,&#39;Segoe UI&#39;,&#39;Roboto&#39;,&#39;Oxygen&#39;,&#39;Ubuntu&#39;,&#39;Cantarell&#39;,&#39;Fira Sans&#39;,&#39;Droid Sans&#39;,&#39;Helvetica Neue&#39;,sans-serif"" cellpadding=""20"" cellspacing=""0"" width=""100%"">
																										<tbody>
																											<tr>
																												<td>
																													<div style=""margin:0;padding:0px;border:none;outline:none;list-style:none;text-align:center;font-family:-apple-system,BlinkMacSystemFont,&#39;Segoe UI&#39;,&#39;Roboto&#39;,&#39;Oxygen&#39;,&#39;Ubuntu&#39;,&#39;Cantarell&#39;,&#39;Fira Sans&#39;,&#39;Droid Sans&#39;,&#39;Helvetica Neue&#39;,sans-serif"">
																														<a href=""https://hirefit.co"" style=""margin-top:0px;margin-bottom:0px;padding:0px;border:medium solid #3bb75e;outline:none;list-style:none;background-color:#3bb75e;color:rgb(255,255,255);display:inline-block;font-size:14px;font-weight:bold;line-height:36px;text-align:center;text-decoration:none;width:150px;border-radius:5px"" target=""_blank"" data-saferedirecturl=""https://hirefit.co"" > Check your Cart</a>
																													</div>
																												</td>
																											</tr>
																										</tbody>
																									</table>
																								</td>
																							</tr>
																						</tbody>
																					</table>
																				</div>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>";

            List<MessageTemplate> messageTemplates = new List<MessageTemplate>() {
                new MessageTemplate
                {
                    Name = "AlertTemplate",
                    Description = "This template is for the customer professional invoice",
                    SenderDisplayName = "Hirefit Cart",
                    SenderEmail = "admin@Hirefit.co",
                    Message = CustomerCartTemplate,
                    Subject = "Abandoned Cart Alert",
                    ClientCopy = "",
                    CreatedDate = DateTime.Now,
                },
            };

            return messageTemplates;
        }
    }
}